﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace lab4
{
    class Connection
    {
        Node node1, node2;
        Panel canvas;
        public int value;

        public Connection(Node n1, Node n2, Panel _canvas)
        {
            node1 = n1;
            node2 = n2;
            canvas = _canvas;
            value = 1;

            draw();
        }

        public void draw()
        {
            Graphics g = canvas.CreateGraphics();
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            Pen pen = new Pen(Color.Black);
            pen.Width = 2;

            g.DrawLine(pen, node1.X, node1.Y, node2.X, node2.Y);
            node1.draw();
            node2.draw();

            g.Dispose();
            pen.Dispose();
        }
    }
}
