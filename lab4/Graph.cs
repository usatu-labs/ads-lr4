﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace lab4
{
    class Graph
    {
        static int COUNT = 12;
        public static int MaxCount
        {
            get { return COUNT; }
        }
        List<Node> nodes = new List<Node>();
        Connection[,] connections = new Connection[COUNT, COUNT];
        Node focusedNode = null;
        Panel canvas;
        Form1 f;

        public int Count
        {
            get { return nodes.Count; }
        }

        public Graph(Panel _canvas, Form1 _f)
        {
            canvas = _canvas;
            f = _f;
            for (int i = 0; i < COUNT; i++)
                for (int j = 0; j < COUNT; j++)
                    connections[i, j] = null;
        }

        public void drawNode(int x, int y)
        {
            resetHighlights();
            if (focusedNode != null) focusedNode.highlight(true);
            if (nodes.Count == COUNT) return;
            Node hitCircle = circleAt(x, y);
            if (hitCircle != null) return;

            Node n = new Node(x, y, (nodes.Count + 1).ToString(), canvas);
            nodes.Add(n);
            n.draw();
            drawTable();
        }

        private Node circleAt(int x, int y)
        {
            for (int i = 0; i < nodes.Count; i++) if (nodes[i].hittest(x, y)) return nodes[i];
            return null;
        }

        public void focusNode(int x, int y)
        {
            resetHighlights();
            if (focusedNode != null) focusedNode.highlight(true);
            Node hitNode = circleAt(x, y);

            if (hitNode == focusedNode)
            {
                if (focusedNode != null) focusedNode.highlight(false);
                focusedNode = null;
                return;
            };

            if (focusedNode == null && hitNode != null)
            {
                focusedNode = hitNode;
                hitNode.highlight(true);
            }
            else if (focusedNode != null && hitNode != null)
            {
                Connection c = new Connection(hitNode, focusedNode, canvas);
                int hitNodeIndex = nodes.FindIndex((Node n) => { return n.ID == hitNode.ID; });
                int focusedNodeIndex = nodes.FindIndex((Node n) => { return n.ID == focusedNode.ID; });
                if (connections[hitNodeIndex, focusedNodeIndex] == null)
                {
                    connections[hitNodeIndex, focusedNodeIndex] = c;
                    connections[focusedNodeIndex, hitNodeIndex] = c;
                }
                
                drawTable();

                focusedNode.highlight(false);
                focusedNode = null;
            }
        }

        public void drawTable()
        {
            f.tableLayoutPanel2.Controls.Clear();
            for (int i = 0; i < nodes.Count; i++)
            {
                Label l1 = new Label();
                Label l2 = new Label();

                l1.Text = nodes[i].Label;
                l2.Text = nodes[i].Label;

                l1.TextAlign = ContentAlignment.MiddleCenter;
                l2.TextAlign = ContentAlignment.MiddleCenter;

                f.tableLayoutPanel2.Controls.Add(l1, i + 1, 0);
                f.tableLayoutPanel2.Controls.Add(l2, 0, i + 1);
            }

            for (int i = 0; i < nodes.Count; i++)
                for (int j = 0; j < nodes.Count; j++)
                {
                    if (connections[i, j] == null) continue;
                    Label t = new Label();
                    t.BackColor = Color.White;
                    t.TextAlign = ContentAlignment.MiddleCenter;
                    t.Text = connections[i, j].value.ToString();
                    f.tableLayoutPanel2.Controls.Add(t, j + 1, i + 1);
                }
        }

        public void clear()
        {
            f.tableLayoutPanel2.Controls.Clear();
            canvas.CreateGraphics().Clear(Color.White);
            nodes.Clear();
            
            for (int i = 0; i < COUNT; i++)
                for (int j = 0; j < COUNT; j++)
                    connections[i, j] = null;       
        }

        private void resetHighlights()
        {
            for (int i = 0; i < nodes.Count; i++) nodes[i].highlight(false);
        }

        public string travel(int startNode)
        {
            resetHighlights();
            if (startNode - 1 >= nodes.Count || nodes.Count == 0) return "";

            Stack<Node> s = new Stack<Node>();
            List<Node> processed = new List<Node>();
            s.Push(nodes[startNode - 1]);

            while (s.Count != 0)            // O(n^3)
            {
                Node removedNode = s.Pop();             // O(1)
                bool isProcessed = processed.FindIndex((Node n) => n.Label == removedNode.Label) != -1; // O(n)
                if (!isProcessed)
                {
                    processed.Add(removedNode);             // O(1)
                    removedNode.highlight(true);
                }

                int removedNodeIndex = nodes.FindIndex((Node n) => n.Label == removedNode.Label);   // O(n)

                for (int i = 0; i < COUNT; i++)     // O(n^2) - dominant
                {
                    if (connections[removedNodeIndex, i] == null) continue; 
                    Node newNode = nodes[i];
                    isProcessed = processed.FindIndex((Node n) => n.Label == newNode.Label) != -1;  // O(n) - dominant
                    if (isProcessed) continue;
                    s.Push(newNode);                                         // O(1)
                }
            }

            string result = "";

            for (int i = 0; i < processed.Count; i++) result += processed[i].Label + ", ";
            return result;
        }
    }


}
